#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:134217728:bb74bf48993bb7c34ce9542b41e35ad8ec5e5a58; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:bb1d15b1d84d5cd81bd0e9a9a9215eaea1ffeb8e \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:134217728:bb74bf48993bb7c34ce9542b41e35ad8ec5e5a58 && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
